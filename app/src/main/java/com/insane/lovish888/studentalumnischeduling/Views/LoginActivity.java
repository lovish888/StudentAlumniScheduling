package com.insane.lovish888.studentalumnischeduling.Views;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.insane.lovish888.studentalumnischeduling.R;
import com.insane.lovish888.studentalumnischeduling.Helpers.TinyDB;

public class LoginActivity extends AppCompatActivity {

    private EditText username, password;
    private Button loginButton;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        loginButton = findViewById(R.id.login_button);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (username.getText().toString().isEmpty())  {
                    Toast.makeText(LoginActivity.this, "Username can't be empty", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (password.getText().toString().isEmpty())  {
                    Toast.makeText(LoginActivity.this, "Password can't be empty", Toast.LENGTH_SHORT).show();
                }

                //Check in Firebase Database
                checkInFirebaseDB();
            }
        });
    }

    private void checkInFirebaseDB() {
        startProgressDialog();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference collectionReference = db.collection("Users");
        Query query = collectionReference
                .whereEqualTo("username", username.getText().toString().toLowerCase())
                .whereEqualTo("password", password.getText().toString());

        query.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                dismissProgressDialog();
                //Login successful
                if (queryDocumentSnapshots.size() > 0) {
                    //Valid user
                    Toast.makeText(LoginActivity.this, "Login successful", Toast.LENGTH_SHORT).show();
                    saveDataToPrefs();
                } else {
                    //Invalid user
                    Toast.makeText(LoginActivity.this, "Incorrect login credentials", Toast.LENGTH_SHORT).show();
                }
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dismissProgressDialog();
                //Login failed
                Toast.makeText(LoginActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void saveDataToPrefs() {
        TinyDB tinyDB = new TinyDB(this);
        tinyDB.putString("username", username.getText().toString().toLowerCase());
        tinyDB.putString("password", password.getText().toString());
        tinyDB.putBoolean("isUserLoggedIn", true);
        startBookingActivity();
    }

    private void startBookingActivity() {
        TinyDB tinyDB = new TinyDB(this);
        if (tinyDB.getString("username").equals("alumni")) {
            startActivity(new Intent(this, BookingRequests.class));
        } else {
            startActivity(new Intent(this, BookAlumniSlot.class));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        TinyDB tinyDB = new TinyDB(this);
        if (tinyDB.getBoolean("isUserLoggedIn")) {
            startBookingActivity();
        }
    }

    private void startProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading ...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
