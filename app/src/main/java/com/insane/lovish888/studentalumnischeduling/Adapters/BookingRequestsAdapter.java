package com.insane.lovish888.studentalumnischeduling.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.GsonBuilder;
import com.insane.lovish888.studentalumnischeduling.Helpers.Constants;
import com.insane.lovish888.studentalumnischeduling.Models.BookingDay;
import com.insane.lovish888.studentalumnischeduling.Models.BookingModel;
import com.insane.lovish888.studentalumnischeduling.R;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Handler;

public class BookingRequestsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private List<BookingModel> bookings = new ArrayList<>();

    public BookingRequestsAdapter(Context context, List<BookingModel> bookings) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.bookings = bookings;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.booking_request_item, parent, false);
        return new BookingRequestView(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        BookingModel current = bookings.get(position);
        BookingRequestView bookingHolder = (BookingRequestView) holder;
        bookingHolder.studentName.setText(current.getBookingBy());
        bookingHolder.date.setText(current.getDate());
        if (current.getBookingTime() == Constants.BOOKING_TIME_12) {
            bookingHolder.timeSlot.setText("1-2 PM");
        } else if (current.getBookingTime() == Constants.BOOKING_TIME_45) {
            bookingHolder.timeSlot.setText("4-5 PM");
        } else {
            bookingHolder.timeSlot.setText("6-7 PM");
        }

        if (current.getBookingStatus() == Constants.BOOKING_ACCEPTED) {
            bookingHolder.bookingStatus.setVisibility(View.VISIBLE);
            bookingHolder.acceptButton.setVisibility(View.GONE);
            bookingHolder.rejectButton.setVisibility(View.GONE);
        } else {
            bookingHolder.bookingStatus.setVisibility(View.GONE);
            bookingHolder.acceptButton.setVisibility(View.VISIBLE);
            bookingHolder.rejectButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return bookings.size();
    }

    class BookingRequestView extends RecyclerView.ViewHolder {

        TextView studentName, date, timeSlot;
        Button acceptButton, rejectButton;
        TextView bookingStatus;

        public BookingRequestView(View itemView) {
            super(itemView);

            studentName = itemView.findViewById(R.id.student_name);
            date = itemView.findViewById(R.id.date);
            timeSlot = itemView.findViewById(R.id.time_slot);
            acceptButton = itemView.findViewById(R.id.accept_button);
            rejectButton = itemView.findViewById(R.id.reject_button);
            bookingStatus = itemView.findViewById(R.id.status);

            acceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getAdapterPosition() >= 0) {
                        BookingModel current = bookings.get(getAdapterPosition());
                        FirebaseFirestore.getInstance().collection("Bookings").document(current.getKey()).update("booking_status", Constants.BOOKING_ACCEPTED);
                    }
                }
            });

            rejectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getAdapterPosition() >= 0) {
                        final BookingModel current = bookings.get(getAdapterPosition());
                        FirebaseFirestore.getInstance().collection("Bookings").document(current.getKey()).update("booking_status", Constants.BOOKING_NONE);
                        new android.os.Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                FirebaseFirestore.getInstance().collection("Bookings").document(current.getKey()).delete();
                            }
                        },2000);
                    }
                }
            });
        }
    }

    //Helper methods
    public void add(BookingModel newBooking) {
        bookings.add(newBooking);
        notifyDataSetChanged();
    }

    public void removeAll() {
        bookings.clear();
        notifyDataSetChanged();
    }
}
