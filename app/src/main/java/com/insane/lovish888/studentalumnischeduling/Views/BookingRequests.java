package com.insane.lovish888.studentalumnischeduling.Views;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.GsonBuilder;
import com.insane.lovish888.studentalumnischeduling.Adapters.BookSlotAdapter;
import com.insane.lovish888.studentalumnischeduling.Adapters.BookingRequestsAdapter;
import com.insane.lovish888.studentalumnischeduling.Models.BookingModel;
import com.insane.lovish888.studentalumnischeduling.R;
import com.insane.lovish888.studentalumnischeduling.Helpers.TinyDB;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.Nullable;

public class BookingRequests extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private BookingRequestsAdapter bookingRequestsAdapter;
    private LinearLayoutManager linearLayoutManager;
    private List<BookingModel> bookings = new ArrayList<>();
    private long todayMidnightTimestamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_requests);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTodayMidnightTimestamp();

        recyclerView = findViewById(R.id.recycler_view);
        bookingRequestsAdapter = new BookingRequestsAdapter(this, bookings);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(bookingRequestsAdapter);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getAllBookings();
            }
        }, 300);
    }

    private void getAllBookings() {
        CollectionReference reference = FirebaseFirestore.getInstance()
                .collection("Bookings");
        reference.whereGreaterThan("timestamp", todayMidnightTimestamp).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.d("Filter", "Listen failed.", e);
                    return;
                }

                if (queryDocumentSnapshots != null) {
                    bookingRequestsAdapter.removeAll();
                    for (QueryDocumentSnapshot snapshot: queryDocumentSnapshots) {
                        BookingModel bookingModel = snapshot.toObject(BookingModel.class);
                        Log.d("Filter", "KEY: " + snapshot.getId());
                        bookingModel.setKey(snapshot.getId());
                        bookingRequestsAdapter.add(bookingModel);
                        //bookingRequestsAdapter.notifyDataSetChanged();
                    }

                }

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_logout) {
            TinyDB tinyDB = new TinyDB(this);
            tinyDB.putBoolean("isUserLoggedIn", false);
            startActivity(new Intent(this, LoginActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setTodayMidnightTimestamp() {
        Calendar date = new GregorianCalendar();
        // reset hour, minutes, seconds and millis
        date.set(Calendar.HOUR_OF_DAY, 23);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        todayMidnightTimestamp = date.getTimeInMillis();
    }
}
