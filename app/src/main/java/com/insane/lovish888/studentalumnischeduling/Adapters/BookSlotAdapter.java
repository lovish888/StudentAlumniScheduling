package com.insane.lovish888.studentalumnischeduling.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.insane.lovish888.studentalumnischeduling.Helpers.Constants;
import com.insane.lovish888.studentalumnischeduling.Models.BookingDay;
import com.insane.lovish888.studentalumnischeduling.R;
import com.insane.lovish888.studentalumnischeduling.Helpers.TinyDB;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookSlotAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private List<BookingDay> bookingDays = new ArrayList<>();

    public BookSlotAdapter(Context context, List<BookingDay> bookingDays) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.bookingDays = bookingDays;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.book_slot_item, parent, false);
        return new BookingDayView(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        BookingDay current = bookingDays.get(position);
        BookingDayView bookingDayHolder = (BookingDayView) holder;
        bookingDayHolder.date.setText(current.getBookingDate());
        bookingDayHolder.day.setText(current.getBookingDay());

        if (current.getTime1Status() == Constants.BOOKING_NONE) {
            bookingDayHolder.time1.setClickable(true);
            bookingDayHolder.status1.setVisibility(View.GONE);
        } else {
            bookingDayHolder.time1.setClickable(false);
            bookingDayHolder.status1.setVisibility(View.VISIBLE);
            if (current.getTime1Status() == Constants.BOOKING_ACCEPTED) {
                bookingDayHolder.status1.setText("Accepted");
            } else if (current.getTime1Status() == Constants.BOOKING_REJECTED) {
                bookingDayHolder.status1.setText("Rejected");
            } else if (current.getTime1Status() == Constants.BOOKING_PENDING) {
                bookingDayHolder.status1.setText("Pending");
            } else {
                bookingDayHolder.status1.setText("Unavailable");
            }
        }

        if (current.getTime2Status() == Constants.BOOKING_NONE) {
            bookingDayHolder.time2.setClickable(true);
            bookingDayHolder.status2.setVisibility(View.GONE);
        } else {
            bookingDayHolder.time2.setClickable(false);
            bookingDayHolder.status2.setVisibility(View.VISIBLE);
            if (current.getTime2Status() == Constants.BOOKING_ACCEPTED) {
                bookingDayHolder.status2.setText("Accepted");
            } else if (current.getTime2Status() == Constants.BOOKING_REJECTED) {
                bookingDayHolder.status2.setText("Rejected");
            } else if (current.getTime2Status() == Constants.BOOKING_PENDING){
                bookingDayHolder.status2.setText("Pending");
            } else {
                bookingDayHolder.status2.setText("Unavailable");
            }
        }

        if (current.getTime3Status() == Constants.BOOKING_NONE) {
            bookingDayHolder.time3.setClickable(true);
            bookingDayHolder.status3.setVisibility(View.GONE);
        } else {
            bookingDayHolder.time3.setClickable(false);
            bookingDayHolder.status3.setVisibility(View.VISIBLE);
            if (current.getTime3Status() == Constants.BOOKING_ACCEPTED) {
                bookingDayHolder.status3.setText("Accepted");
            } else if (current.getTime3Status() == Constants.BOOKING_REJECTED) {
                bookingDayHolder.status3.setText("Rejected");
            } else if (current.getTime3Status() == Constants.BOOKING_PENDING){
                bookingDayHolder.status3.setText("Pending");
            } else {
                bookingDayHolder.status3.setText("Unavailable");
            }
        }

    }

    @Override
    public int getItemCount() {
        return bookingDays.size();
    }

    class BookingDayView extends RecyclerView.ViewHolder {

        TextView date, day;
        Button time1, time2, time3;
        TextView status1, status2, status3;

        public BookingDayView(View itemView) {
            super(itemView);

            date = itemView.findViewById(R.id.date);
            day = itemView.findViewById(R.id.day);
            time1 = itemView.findViewById(R.id.time1);
            time2 = itemView.findViewById(R.id.time2);
            time3 = itemView.findViewById(R.id.time3);
            status1 = itemView.findViewById(R.id.status1);
            status2 = itemView.findViewById(R.id.status2);
            status3 = itemView.findViewById(R.id.status3);

            time1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getAdapterPosition() >= 0 && Constants.isBookingAllowed) {
                        BookingDay current  = bookingDays.get(getAdapterPosition());
                        addBooking(current, Constants.BOOKING_TIME_12, getAdapterPosition());
                    }
                }
            });

            time2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getAdapterPosition() >= 0 && Constants.isBookingAllowed) {
                        BookingDay current  = bookingDays.get(getAdapterPosition());
                        addBooking(current, Constants.BOOKING_TIME_45, getAdapterPosition());
                    }
                }
            });

            time3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getAdapterPosition() >= 0 && Constants.isBookingAllowed) {
                        BookingDay current  = bookingDays.get(getAdapterPosition());
                        addBooking(current, Constants.BOOKING_TIME_67, getAdapterPosition());
                    }
                }
            });
        }
    }

    private void addBooking(BookingDay bookingDay, int bookingTime, int position) {

        CollectionReference reference = FirebaseFirestore.getInstance()
                .collection("Bookings");

        TinyDB tinyDB = new TinyDB(context);

        Map<String, Object> data = new HashMap<>();
        data.put("booking_by", tinyDB.getString("username"));
        data.put("booking_status", Constants.BOOKING_PENDING);
        data.put("date", bookingDay.getBookingDate());
        data.put("timestamp", bookingDayTimestamp(position));
        data.put("booking_time", bookingTime);

        reference.document().set(data)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("Filter", "Saved successfully");
                    }
                });

    }

    private long bookingDayTimestamp(int position) {
        final long ONE_DAY_MILLI_SECONDS = 25 * 60 * 60 * 1000;
        return System.currentTimeMillis() + (position + 1) * ONE_DAY_MILLI_SECONDS;
    }
}
