package com.insane.lovish888.studentalumnischeduling.Models;

public class BookingDay {

    private String bookingDay;
    private String bookingDate;
    private int time1Status = 0;
    private int time2Status = 0;
    private int time3Status = 0;

    public int getTime1Status() {
        return time1Status;
    }

    public void setTime1Status(int time1Status) {
        this.time1Status = time1Status;
    }

    public int getTime2Status() {
        return time2Status;
    }

    public void setTime2Status(int time2Status) {
        this.time2Status = time2Status;
    }

    public int getTime3Status() {
        return time3Status;
    }

    public void setTime3Status(int time3Status) {
        this.time3Status = time3Status;
    }

    public String getBookingDay() {
        return bookingDay;
    }

    public void setBookingDay(String bookingDay) {
        this.bookingDay = bookingDay;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }
}
