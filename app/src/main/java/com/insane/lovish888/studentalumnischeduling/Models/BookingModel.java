package com.insane.lovish888.studentalumnischeduling.Models;

import com.google.firebase.firestore.PropertyName;
import com.google.gson.annotations.SerializedName;

public class BookingModel {

    @SerializedName("booking_by")
    private String bookingBy;
    @SerializedName("booking_status")
    private int bookingStatus;
    @SerializedName("date")
    private String date;
    @SerializedName("timestamp")
    private long timestamp;
    @SerializedName("booking_time")
    private int bookingTime;
    private String key;

    @PropertyName("booking_by")
    public String getBookingBy() {
        return bookingBy;
    }

    @PropertyName("booking_by")
    public void setBookingBy(String bookingBy) {
        this.bookingBy = bookingBy;
    }

    @PropertyName("booking_status")
    public int getBookingStatus() {
        return bookingStatus;
    }

    @PropertyName("booking_status")
    public void setBookingStatus(int bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @PropertyName("booking_time")
    public int getBookingTime() {
        return bookingTime;
    }

    @PropertyName("booking_time")
    public void setBookingTime(int bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
