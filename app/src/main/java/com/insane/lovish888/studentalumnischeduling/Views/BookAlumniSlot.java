package com.insane.lovish888.studentalumnischeduling.Views;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.GsonBuilder;
import com.insane.lovish888.studentalumnischeduling.Adapters.BookSlotAdapter;
import com.insane.lovish888.studentalumnischeduling.Helpers.Constants;
import com.insane.lovish888.studentalumnischeduling.Models.BookingDay;
import com.insane.lovish888.studentalumnischeduling.Models.BookingModel;
import com.insane.lovish888.studentalumnischeduling.R;
import com.insane.lovish888.studentalumnischeduling.Helpers.TinyDB;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.Nullable;

public class BookAlumniSlot extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private BookSlotAdapter bookSlotAdapter;
    private LinearLayoutManager linearLayoutManager;
    private TextView bookingAvailabilityText;
    List<BookingDay> bookingDays = new ArrayList<>();
    private long todayMidnightTimestamp;
    TinyDB tinyDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_alumni_slot);

        tinyDB = new TinyDB(this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTodayMidnightTimestamp();
        bookingAvailabilityText = findViewById(R.id.booking_availability_text);
        bookingAvailabilityText.setVisibility(View.GONE);

        recyclerView = findViewById(R.id.recycler_view);
        bookSlotAdapter = new BookSlotAdapter(this, bookingDays);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(bookSlotAdapter);

        getDays();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getBookingFromFirebase();
            }
        }, 300);
    }

    private void setTodayMidnightTimestamp() {
        Calendar date = new GregorianCalendar();
        // reset hour, minutes, seconds and millis
        date.set(Calendar.HOUR_OF_DAY, 23);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        todayMidnightTimestamp = date.getTimeInMillis();
    }

    private void getBookingFromFirebase() {
        CollectionReference reference = FirebaseFirestore.getInstance()
                .collection("Bookings");
        reference.whereGreaterThan("timestamp", todayMidnightTimestamp).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.d("Filter", "Listen failed.", e);
                    return;
                }

                List<BookingModel> bookings = new ArrayList<>();

                if (queryDocumentSnapshots != null) {
                    bookings = queryDocumentSnapshots.toObjects(BookingModel.class);
                    //Log.d("Filter", new GsonBuilder().setPrettyPrinting().create().toJson(bookings));
                    updateBookingsOnUI(bookings);
                }

            }
        });
    }


    private void updateBookingsOnUI(List<BookingModel> bookings) {

        boolean isPendingBooking = false;
        int numberOfAcceptedBookings = 0;

        for (BookingModel booking: bookings) {

            if (booking.getBookingStatus() == Constants.BOOKING_PENDING) {
                //Booking is already pending, disable further bookings
                isPendingBooking = true;
            }

            if (booking.getBookingBy().equals(tinyDB.getString("username"))) {
                for (BookingDay bookingDay: bookingDays) {
                    if (bookingDay.getBookingDate().equals(booking.getDate())) {
                        if (booking.getBookingTime() == Constants.BOOKING_TIME_12) {
                            bookingDay.setTime1Status(booking.getBookingStatus());
                        } else if (booking.getBookingTime() == Constants.BOOKING_TIME_45) {
                            bookingDay.setTime2Status(booking.getBookingStatus());
                        } else {
                            bookingDay.setTime3Status(booking.getBookingStatus());
                        }
                        if (booking.getBookingStatus() == Constants.BOOKING_ACCEPTED) {
                            numberOfAcceptedBookings++;
                        }
                        bookSlotAdapter.notifyDataSetChanged();
                        break;
                    }
                }
            } else {
                //Booking is by other student, disable booked slots
                for (BookingDay bookingDay: bookingDays) {
                    if (bookingDay.getBookingDate().equals(booking.getDate())) {
                        if (booking.getBookingTime() == Constants.BOOKING_TIME_12) {
                            if (booking.getBookingStatus() == Constants.BOOKING_NONE) {
                                bookingDay.setTime1Status(Constants.BOOKING_NONE);
                            } else {
                                bookingDay.setTime1Status(Constants.BOOKING_UNAVAILABLE);
                            }

                        } else if (booking.getBookingTime() == Constants.BOOKING_TIME_45) {
                            if (booking.getBookingStatus() == Constants.BOOKING_NONE) {
                                bookingDay.setTime2Status(Constants.BOOKING_NONE);
                            } else {
                                bookingDay.setTime2Status(Constants.BOOKING_UNAVAILABLE);
                            }

                        } else {
                            if (booking.getBookingStatus() == Constants.BOOKING_NONE) {
                                bookingDay.setTime3Status(Constants.BOOKING_NONE);
                            } else {
                                bookingDay.setTime3Status(Constants.BOOKING_UNAVAILABLE);
                            }
                        }
                        bookSlotAdapter.notifyDataSetChanged();
                        break;
                    }
                }
            }
        }

        if (isPendingBooking) {
            Constants.isBookingAllowed = false;
            bookingAvailabilityText.setVisibility(View.VISIBLE);
        } else {
            Constants.isBookingAllowed = true;
            bookingAvailabilityText.setVisibility(View.GONE);
        }

        if (numberOfAcceptedBookings == 2) {
            Constants.isBookingAllowed = false;
            bookingAvailabilityText.setText("Further Booking not allowed. Maximum booking limit reached.");
            bookingAvailabilityText.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_logout) {
            tinyDB.putBoolean("isUserLoggedIn", false);
            startActivity(new Intent(this, LoginActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getDays() {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE dd-MMM-yyyy");
        for (int i = 1; i < 8; i++) {
            Calendar calendar = new GregorianCalendar();
            calendar.add(Calendar.DATE, i);
            String day = sdf.format(calendar.getTime());
            BookingDay bookingDay = new BookingDay();
            bookingDay.setBookingDay(day.substring(0,day.indexOf(" ")));
            bookingDay.setBookingDate(day.substring(day.indexOf(" ") + 1));
            bookingDays.add(bookingDay);
            Log.d("Filter", String.valueOf(bookingDay));
        }
        bookSlotAdapter.notifyDataSetChanged();
    }
}
