package com.insane.lovish888.studentalumnischeduling.Helpers;

public class Constants {
    public static final int BOOKING_ACCEPTED = 1;
    public static final int BOOKING_REJECTED = 2;
    public static final int BOOKING_PENDING = 3;
    public static final int BOOKING_NONE = 0;
    public static final int BOOKING_UNAVAILABLE = -1;

    public static final int BOOKING_TIME_12 = 1;
    public static final int BOOKING_TIME_45 = 2;
    public static final int BOOKING_TIME_67 = 3;

    public static boolean isBookingAllowed = true;
}
